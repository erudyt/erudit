<?php

/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$installer->startSetup();

$connection = $installer->getConnection();

// Delete Panalysis_TagManager config data, if any

$connection->delete(
    $this->getTable('core_config_data'),
    $connection->prepareSqlCondition('path', array(
        'like' => 'panalysis_tagmanager/gtm/%'
    ))
);

// Set up Panalysis_TagManager config data

$installer->setConfigData('panalysis_tagmanager/gtm/containerid', "GTM-T84QMP6");
$installer->setConfigData('panalysis_tagmanager/gtm/brandcode', "manufacturer");
$installer->setConfigData('panalysis_tagmanager/gtm/multiple_currencies', '0');
$installer->setConfigData('panalysis_tagmanager/gtm/enable_product_lists', '1');
$installer->setConfigData('panalysis_tagmanager/gtm/max_products', '100');
$installer->setConfigData('panalysis_tagmanager/gtm/enable_ajax', '1');
$installer->setConfigData('panalysis_tagmanager/gtm/start_funnel_at_cart', '0');
$installer->setConfigData('panalysis_tagmanager/gtm/use_customcheckout', "1");
$installer->setConfigData('panalysis_tagmanager/gtm/checkout_module', "Apptha_Onestepcheckout");
$installer->setConfigData('panalysis_tagmanager/gtm/checkout_controller', "onestepcheckout");
$installer->setConfigData('panalysis_tagmanager/gtm/checkout_action', "index");

$installer->endSetup();
