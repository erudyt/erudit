<?php

class Ago_Catalog_Model_Product extends Mage_Catalog_Model_Product
{

//Наиболее важный пункт: не должно дублироваться содержимое поля "Адрес" во вкладке "Meta информация".
//Было бы неплохо, если бы можно было сделать, чтобы обнулялись следующие пункты:
//Характеристики - ISBN
//Изображения
//
//а во вкладке "Склад" сразу проставлялись данные:
//"Кол-во" - 1
//"Наличие на складе" - в наличии
//Но это программа максимум. Как минимум, хотелось бы, чтобы не дублировалось содержимое поля "Адрес" во вкладке "Meta информация".
    /**
     * Create duplicate
     *
     * @return Mage_Catalog_Model_Product
     */
    public function duplicate()
    {
//        return parent::duplicate();
        $this->getWebsiteIds();
        $this->getCategoryIds();
        $data = $this->getData();

//        $data['url_key'] = $data['url_key'].'-mpt';
//        $data['url_path'] = " ";
        $data['isbn'] = null;
        $data['image'] = null;
        $data['small_image'] = null;
        $data['foto_slider'] = null;
        $data['thumbnail'] = null;


        if (isset($data['media_gallery']['images'])) {
            $data['media_gallery']['images'] = array();
        }

        /* @var $newProduct Mage_Catalog_Model_Product */
        $newProduct = Mage::getModel('catalog/product')->setData($data)
            ->setIsDuplicate(true)
            ->setOriginalId($this->getId())
            ->setSku(null)
//            ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
            ->setCreatedAt(null)
            ->setUpdatedAt(null)
            ->setId(null)
            ->setStoreId(Mage::app()->getStore()->getId());

//        $newProduct->setData('duplicated', true);
//        $newProduct->setUrlKey(false);

//        $newProduct->setUrlKey(' ');
//        $newProduct->setData('url_key', ' ');

//        $newProduct->setIsDuplicable();
        Mage::dispatchEvent(
            'catalog_model_product_duplicate',
            array('current_product' => $this, 'new_product' => $newProduct)
        );

        /* Prepare Related*/
        $data = array();
        $this->getLinkInstance()->useRelatedLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getRelatedLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }
        $newProduct->setRelatedLinkData($data);

        /* Prepare UpSell*/
        $data = array();
        $this->getLinkInstance()->useUpSellLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getUpSellLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }

        $newProduct->setUpSellLinkData($data);

        /* Prepare Cross Sell */
        $data = array();
        $this->getLinkInstance()->useCrossSellLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getCrossSellLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }

        $newProduct->setCrossSellLinkData($data);

        /* Prepare Grouped */
        $data = array();
        $this->getLinkInstance()->useGroupedLinks();
        $attributes = array();
        foreach ($this->getLinkInstance()->getAttributes() as $_attribute) {
            if (isset($_attribute['code'])) {
                $attributes[] = $_attribute['code'];
            }
        }
        foreach ($this->getGroupedLinkCollection() as $_link) {
            $data[$_link->getLinkedProductId()] = $_link->toArray($attributes);
        }


        $newProduct->setGroupedLinkData($data);
        $newProduct->setStockData(array('is_in_stock' => 1, 'qty' => 1));
        $newProduct->save();


        $resource = Mage::getSingleton('core/resource');
        $writeConnection = $resource->getConnection('core_write');
        $query = "delete from catalog_product_entity_media_gallery where entity_id = ".$newProduct->getId();
        $writeConnection->query($query);

        $this->getOptionInstance()->duplicate($this->getId(), $newProduct->getId());
        $this->getResource()->duplicate($this->getId(), $newProduct->getId());
        $newProduct->setData('duplicated', 1)->getResource()->saveAttribute($newProduct, 'duplicated');

        $newProduct->setData('url_key', '')->getResource()->saveAttribute($newProduct, 'url_key');

        // TODO - duplicate product on all stores of the websites it is associated with
        /*if ($storeIds = $this->getWebsiteIds()) {
            foreach ($storeIds as $storeId) {
                $this->setStoreId($storeId)
                   ->load($this->getId());

                $newProduct->setData($this->getData())
                    ->setSku(null)
                    ->setStatus(Mage_Catalog_Model_Product_Status::STATUS_DISABLED)
                    ->setId($newId)
                    ->save();
            }
        }*/


        return $newProduct;
    }
}
