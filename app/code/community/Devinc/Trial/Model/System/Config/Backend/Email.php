<?php

class Devinc_Trial_Model_System_Config_Backend_Email extends Mage_Core_Model_Config_Data
{    
    public function _beforeSave()
    {        
    	if ($this->getValue()!='') {
    		$error = false;
    		if (!Zend_Validate::is(trim($this->getValue()), 'EmailAddress')) {
                    $error = true;
            }

            if ($error) {
				throw new Mage_Core_Exception('Please specify a valid email address.');
            } else {
    			Mage::register('customer_email', $this->getValue());
    		}
    	} else {
			throw new Mage_Core_Exception('Please specify an email address.');
	    }
    }    
}
