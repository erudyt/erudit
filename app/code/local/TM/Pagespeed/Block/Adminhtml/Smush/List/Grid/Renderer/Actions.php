<?php

class TM_Pagespeed_Block_Adminhtml_Smush_List_Grid_Renderer_Actions
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Renders grid column
     *
     * @param   Varien_Object $row
     * @return  string
     */
    public function render(Varien_Object $row)
    {
        $links = array();

        $links[] = sprintf(
            '<a href="%s">%s</a>',
            $this->getUrl('*/*/restore/', array('_current' => true, 'id' => $row->getId())),
            Mage::helper('TM_Pagespeed')->__('Restore Origin')
        );

        $links[] = sprintf(
            '<a href="%s">%s</a>',
            $this->getUrl('*/*/menualsmush/', array('_current' => true, 'id' => $row->getId())),
            Mage::helper('TM_Pagespeed')->__('Manual Optimize')
        );

        $links[] = sprintf(
            '<a href="%s">%s</a>',
            $this->getUrl('*/*/resmush/', array('_current' => true, 'id' => $row->getId())),
            Mage::helper('TM_Pagespeed')->__('Re-Optimize Origin')
        );

        // $links[] = sprintf(
        //     '<a href="%s">%s</a>',
        //     $this->getUrl('*/*/delete/', array('_current' => true, 'id' => $row->getId())),
        //     Mage::helper('TM_Pagespeed')->__('Delete record from database')
        // );

        return implode(" |\n", $links);
    }
}
