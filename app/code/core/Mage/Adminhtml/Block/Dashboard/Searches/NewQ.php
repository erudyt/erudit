<?php

class Mage_Adminhtml_Block_Dashboard_Searches_NewQ extends Mage_Adminhtml_Block_Dashboard_Grid
{
    protected $_collection;

    public function __construct()
    {
        parent::__construct();
        $this->setId('newQGrid');
    }

    protected function _prepareCollection()
    {
        $this->_collection = Mage::getModel('testimonial/testimonial')->getCollection();
        $this->_collection->setOrder('created_time', 'DESC');
        $this->_collection->addFieldToFilter('status', 3);
        $this->_collection->setPageSize(5);

        $this->setCollection($this->_collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    => 'Имя',
            'sortable'  => false,
            'index'     => 'name',
            'renderer'  => 'adminhtml/dashboard_searches_renderer_searchquery',
        ));

        $this->addColumn('email', array(
            'header'    => 'Электронная почта (email)',
            'sortable'  => false,
            'index'     => 'email',
            'renderer'  => 'adminhtml/dashboard_searches_renderer_searchquery',
        ));

        $this->addColumn('testimonial', array(
            'header'    => 'Вопрос',
            'sortable'  => false,
            'index'     => 'testimonial',
            'renderer'  => 'adminhtml/dashboard_searches_renderer_searchquery',
        ));

        $this->addColumn('created_time', array(
            'header'    => 'Создан',
            'sortable'  => false,
            'index'     => 'created_time',
            'type'      => 'datetime'
        ));

        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('testimonial/adminhtml_testimonial/edit', array('id'=>$row->getId()));
    }
}
