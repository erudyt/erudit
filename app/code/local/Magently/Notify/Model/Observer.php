<?php

class Magently_Notify_Model_Observer {
    /**

     * Adds notify button to static block edition form.

     *
     * @event adminhtml_widget_container_html_before
     */
    public function beforeContainerHtml($observer)
    {
        $block = $observer->getBlock();

        if ($block instanceof Mage_Adminhtml_Block_Cms_Block_Edit) {
            $product = Mage::registry('current_product');

            $block->addButton('magently_notify_duplicate', array(
                'label'     => Mage::helper('adminhtml')->__('Duplicate'),
                'onclick'   => 'setLocation(\'' . $block->getUrl('*/magently_notify/duplicate/block_id/' . $block_id) . '\')',
                'class'     => 'duplicate',
            ), -1, 0);
        }
    }
}