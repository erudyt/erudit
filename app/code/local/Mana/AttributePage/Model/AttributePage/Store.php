<?php
/** 
 * @category    Mana
 * @package     Mana_AttributePage
 * @copyright   Copyright (c) http://www.manadev.com
 * @license     http://www.manadev.com/license  Proprietary License
 */
/**
 * @author Mana Team
 *
 */
class Mana_AttributePage_Model_AttributePage_Store extends Mana_AttributePage_Model_AttributePage_Abstract {
    const ENTITY = 'mana_attributepage/attributePage_store';

    protected function _construct() {
        $this->_init(self::ENTITY);
    }

    public function canShow() {
        return $this->getData('is_active');
    }

    public function getUrl() {
        return Mage::getUrl('mana/attributePage/view', array(
            'id' => $this->getId(),
            '_use_rewrite' => true,
            '_secure' => Mage::app()->getFrontController()->getRequest()->isSecure(),
        ));
    }

    public function loadByOptionPageStoreId($optionPageStoreId) {
        if ($id = $this->getResource()->getIdByOptionPageStoreId($optionPageStoreId)) {
            $this->load($id);
        }

        return $this;
    }

    /**
     * @return Mana_AttributePage_Resource_OptionPage_Store_Collection
     */
    public function getOptionPages() {
        $collection = $this->createOptionPageCollection();
        $collection
            ->addAttributePageFilter($this->getData('attribute_page_global_id'))
            ->addStoreFilter($this->getData('store_id'))
            ->addIsActiveFilter()
            ->setOrder('title', 'ASC');

        $collection->addAlphaColumn();

        return $collection;
    }

    /**
     * @return Mana_AttributePage_Resource_OptionPage_Store_Collection
     */
    public function getOptionFeatured() {
        $collection = $this->createOptionPageCollection()
            ->addAttributePageFilter($this->getData('attribute_page_global_id'))
            ->addStoreFilter($this->getData('store_id'))
            ->addFeaturedFilter()
            ->setOrder('position', 'ASC');
		
        return $collection;
    }
	    public function getOptionFeaturedHome($store_id) {
        $collection = $this->createOptionPageCollection()
            ->addAttributePageFilter('1')
            ->addStoreFilter($store_id)
            ->addFeaturedFilter()
            
            //->addFieldToFilter("is_featured", array('eq' => 1))
            
            ->setOrder('position', 'ASC');
			
        return $collection;
    }

	public function getBestsellingProductHome($id_manuf)
    {
        // todo::uvlek
        $productCount = 5;
        $storeId    = Mage::app()->getStore()->getId();
        $missingProducts = array();

        // get today and last 30 days time
        $today = time();
        $last = $today - (60*60*24*30);

        $from = date("Y-m-d", $last);
        $to = date("Y-m-d", $today);

        // get most viewed products for current category
        $products = Mage::getResourceModel('reports/product_collection')
            ->addAttributeToSelect(array('id', 'name', 'price', 'small_image', 'avtor'))
            ->addOrderedQty($from, $to)
            ->setStoreId($storeId)
            ->addStoreFilter($storeId)
            ->setOrder('ordered_qty', 'desc')
            ->setPageSize($productCount);

        // $products->getSelect()->joinInner(array('e2' => 'catalog_product_flat_'.$storeId), 'e2.entity_id = e.entity_id');
        $products->addAttributeToFilter('avtor',array('eq'=> $id_manuf));
        Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
        Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);

        $countProducts = count($products);
        if (5 != $countProducts) {
            $storeId    = Mage::app()->getStore()->getId();
            $missingProducts = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->addAttributeToSelect(array('id', 'name', 'price', 'small_image', 'avtor'))
                ->setStoreId($storeId)
                ->addStoreFilter($storeId);

            $ids = array_map(function($product){
                return $product->id;
            }, $products);

            $missingProducts->addAttributeToFilter('entity_id', array('nin' => $ids));
            $missingProducts ->addAttributeToFilter('avtor',array('eq'=> $id_manuf));

            Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($missingProducts );
            Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($missingProducts );

            $missingProducts->setPageSize(5 - $countProducts)->setCurPage(1);
//            $products = array($products, $missingProducts);
        }

        return array($products, $missingProducts);

        //echo $products->getSelect();
        // echo count($products);
        if (count($products)==0){
            $products_ = Mage::getResourceModel('reports/product_collection')->addAttributeToSelect('*')
                ->setStoreId($storeId)
                ->addStoreFilter($storeId)
                ->addViewsCount()
                ->setPageSize($productCount);
            $products_->addAttributeToFilter('avtor',array('eq'=> $id_manuf));
            Mage::getSingleton('catalog/product_status') ->addVisibleFilterToCollection($products_);
            Mage::getSingleton('catalog/product_visibility') ->addVisibleInCatalogFilterToCollection($products_);
            //echo $products_->getSelect();
            return $products_;
        } else {
            return $products;
        }
    }

    /**
     * @return Mana_AttributePage_Resource_AttributePage_Store
     */
    public function getResource() {
        return parent::getResource();
    }

    #region Dependencies
    /**
     * @return Mana_AttributePage_Resource_OptionPage_Store_Collection
     */
    public function createOptionPageCollection() {
        return Mage::getResourceModel('mana_attributepage/optionPage_store_collection');
    }
    #endregion
}