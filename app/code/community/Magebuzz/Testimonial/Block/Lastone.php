<?php

class Magebuzz_Testimonial_Block_Lastone extends AW_Blog_Block_Menu_Sidebar
{
    public function getLastOne()
    {
        $collection = Mage::getModel('testimonial/testimonial')->getCollection();
        $collection->setOrder('created_time', 'DESC');
        $collection->addFieldToFilter('status',1);
        $collection->setPageSize(5);

        return $collection;
    }

    public function getUrl() {
        $url = '';
        $uri = Mage::helper('core/url')->getCurrentUrl();
        if (preg_match('/\/ua\//',$uri)) {
            $url .= '/ua';
        }
        if (preg_match('/\/ru\//',$uri)) {
            $url .= '/ru';
        }
        $url .= '/testimonial';

        return $url;
    }
}