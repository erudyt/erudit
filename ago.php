<?php


define('MAGENTO_ROOT', getcwd());

$compilerConfig = MAGENTO_ROOT . '/includes/config.php';
if (file_exists($compilerConfig)) {
    include $compilerConfig;
}

$mageFilename = MAGENTO_ROOT . '/app/Mage.php';
require_once $mageFilename;

/* Store or website code */ $mageRunCode = isset($_SERVER['MAGE_RUN_CODE']) ? $_SERVER['MAGE_RUN_CODE'] : '';

/* Run store or run website */ $mageRunType = isset($_SERVER['MAGE_RUN_TYPE']) ? $_SERVER['MAGE_RUN_TYPE'] : 'store';

Mage::init($mageRunCode, $mageRunType, array());

$mailTemplate = Mage::getModel('core/email_template');
$sender = array(
    'name' => 'Developer',
    'email' => 'info@erudyt.com.ua',
);

$mailTemplate->setDesignConfig(array('area' => 'frontend'))->sendTransactional(
    Mage::getStoreConfig('sendfriend/email/template'),
//    Mage::getStoreConfig('customer/create_account/email_confirmation_template'),
    $sender,
    'web-Y6RHfb@mail-tester.com',
    'Name',
    array(
        'name'          => 'name',
        'email'         => 'uvleek@gmail.com',
        'product_name'  => 'uvleek@gmail.com',
        'product_url'   => 'http://google.com/',
        'message'       => 'uvleek@gmail.com',
        'sender_name'   => 'uvleek@gmail.com',
        'sender_email'  => 'uvleek@gmail.com',
        'product_image' => 'uvleek@gmail.com'
    )

//    {{var customer.name}} @-->
//<!--@vars
//{"store url=\"\"":"Store Url",
//"skin url=\"images/logo_email.gif\" _area='frontend'":"Email Logo Image",
//"store url=\"customer/account/\"":"Customer Account Url",
//"htmlescape var=$customer.name":"Customer Name",
//"var customer.email":"Customer Email",
//"store url=\"customer/account/confirm/\" _query_id=$customer.id _query_key=$customer.confirmation _query_back_url=$back_url":"Confirmation Url",
//"htmlescape var=$customer.password":"Customer password"}
);