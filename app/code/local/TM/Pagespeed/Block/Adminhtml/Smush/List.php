<?php
class TM_Pagespeed_Block_Adminhtml_Smush_List extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'tm_pagespeed';
        $this->_controller = 'adminhtml_smush_list';

        $this->_headerText = Mage::helper('TM_Pagespeed')->__('Image optimisation');
        parent::__construct();
        $this->_removeButton('add');

        $this->_addButton('images', array(
            'label'     => Mage::helper('TM_Pagespeed')->__('Not optimized images'),
            'onclick'   => "setLocation('" . $this->getUrl('*/*/images') . "')",
            'class'     => 'save',
        ), -100);

        $this->_addButton('refresh', array(
            'label'     => Mage::helper('TM_Pagespeed')->__('Process manually'),
            'onclick'   => "setLocation('" . $this->getUrl('*/*/smush') . "')",
            'class'     => 'save',
        ), -100);
    }
}