<?php

// $minifyLibPath = BP . DS . 'lib' . DS . 'pagespeed' . DS . 'min';
// set_include_path( $minifyLibPath . PATH_SEPARATOR . get_include_path());
//
require_once 'pagespeed/min/Minify/HTML.php';
require_once 'pagespeed/min/Minify/CSS.php';
require_once 'pagespeed/min/Minify/CSS/Compressor.php';
require_once 'pagespeed/min/Minify/CommentPreserver.php';
require_once 'pagespeed/min/Minify/CSS/UriRewriter.php';
require_once 'pagespeed/min/JSMin.php';

class TM_Pagespeed_Model_Minify_Html extends Minify_HTML
{
    const MINIFY_CONTENT_ENABLE_JS_CONFIG  = 'tm_pagespeed/minify_content/enabled_js';
    const MINIFY_CONTENT_ENABLE_CSS_CONFIG = 'tm_pagespeed/minify_content/enabled_css';

    public static function liteMin($html)
    {
        $min = new self($html);
        // trim js code
        $html = preg_replace_callback(
            '/(\\s*)<script(\\b[^>]*?>)([\\s\\S]*?)<\\/script>(\\s*)/i'
            ,array($min, '_liteRemoveScriptCB')
            ,$html);

        // Trim each line
        $html = preg_replace('/^\\s+|\\s+$/m', '', $html);
        // Remove HTML comments
        $html =  preg_replace_callback(
            '/<!--([\\s\\S]*?)-->/',
            array($min, '_commentCB'),
            $html);
        // Remove ws around block/undisplayed elements
        $html = preg_replace('/\\s+(<\\/?(?:area|base(?:font)?|blockquote|body'
            .'|caption|center|cite|col(?:group)?|dd|dir|div|dl|dt|fieldset|form'
            .'|frame(?:set)?|h[1-6]|head|hr|html|legend|li|link|map|menu|meta'
            .'|ol|opt(?:group|ion)|p|param|t(?:able|body|head|d|h||r|foot|itle)'
            .'|ul)\\b[^>]*>)/i', '$1', $html);
        // Remove ws outside of all elements
        $html = preg_replace_callback(
            '/>([^<]+)</',
            array($min, '_outsideTagCB'),
            $html);

        $html = str_replace("\t", " ", $html);
        $html = str_replace("\n", " ", $html);
        $html = str_replace("\r", " ", $html);

        $html = str_replace("  ", " ", $html);
        return $html;
    }

    public static function min($html)
    {
        $options = array();
        $isCssMinifier = (bool) Mage::getStoreConfig(self::MINIFY_CONTENT_ENABLE_CSS_CONFIG);
        if ($isCssMinifier) {
            $options['cssMinifier'] = array('Minify_CSS', 'minify');
        }
        $isJsMinifier = (bool) Mage::getStoreConfig(self::MINIFY_CONTENT_ENABLE_JS_CONFIG);
        if ($isJsMinifier) {
            $options['jsMinifier'] = array('JSMin', 'minify');
        }
        //$html = Minify_HTML::minify($html, $options);
        $html = self::minify($html, $options);

        return $html;
    }

    public static function minify($html, $options = array())
    {
        $min = new self($html, $options);
        return $min->process();
    }

    protected function _commentCB($m)
    {
        return (0 === strpos($m[1], '[')
            || false !== strpos($m[1], '<![')
            || false !== strpos($m[1], 'ajaxpro_')
            || false !== stripos($m[1], 'esi <')
            || false !== stripos($m[1], ' fpc')
            )
            ? $m[0]
            : '';
    }

    protected function _outsideTagCB($m)
    {
        return '>' . preg_replace('/^\\s+|\\s+$/', ' ', $m[1]) . '<';
    }

    protected function _liteRemoveScriptCB($m)
    {
        $openScript = "<script{$m[2]}";
        $js = $m[3];

        // whitespace surrounding? preserve at least one space
        $ws1 = ($m[1] === '') ? '' : ' ';
        $ws2 = ($m[4] === '') ? '' : ' ';

        $js = preg_replace('/(?:^\\s*<!--\\s*|\\s*(?:\\/\\/)?\\s*-->\\s*$)/', '', $js);
        $js = preg_replace( "/(?<!\:)\/\/(.*)\\n/", "", $js); // strip line comments

        // $replace = array(
        //     '#\'([^\n\']*?)/\*([^\n\']*)\'#' => "'\1/'+\'\'+'*\2'", // remove comments from ' strings
        //     '#\"([^\n\"]*?)/\*([^\n\"]*)\"#' => '"\1/"+\'\'+"*\2"', // remove comments from " strings
        //     '#/\*.*?\*/#s'            => "",      // strip C style comments
        //     '#[\r\n]+#'               => "\n",    // remove blank lines and \r's
        //     '#\n([ \t]*//.*?\n)*#s'   => "\n",    // strip line comments (whole line only)
        //     '#([^\\])//([^\'"\n]*)\n#s' => "\\1\n",
        //                                           // strip line comments
        //                                           // (that aren't possibly in strings or regex's)
        //     '#\n\s+#'                 => "\n",    // strip excess whitespace
        //     '#\s+\n#'                 => "\n",    // strip excess whitespace
        //     '#(//[^\n]*\n)#s'         => "\\1\n", // extra line feed after any comments left
        //                                           // (important given later replacements)
        //     '#/([\'"])\+\'\'\+([\'"])\*#' => "/*" // restore comments in strings
        // );
        // $js = preg_replace(array_keys($replace), $replace, $js);

        return trim("{$ws1}{$openScript}{$js}</script>{$ws2}");
    }
}