<?php
class TM_Pagespeed_Model_System_Config_Source_Minify_Js_Type
{
    const CLOSURE_COMPILER_TYPE = 'closure compiler';
    const YUI_TYPE              = 'yui compressor';
    const JSMIN_TYPE            = 'jsmin';
    const JSMINPLUS_TYPE        = 'jsminplus';

    /**
     *
     * @return array
     */
    public function toOptionHash()
    {
        return $this->_toOptionHash();
    }

    /**
     *
     * @return array
     */
    protected function _toOptionHash()
    {
        $result = array();
        $consts = array(self::JSMIN_TYPE, self::JSMINPLUS_TYPE);

        if(function_exists('exec')) {
            $consts[] = self::YUI_TYPE;
            $consts[] = self::CLOSURE_COMPILER_TYPE;
        }

        $model = Mage::getModel('TM_Pagespeed/minify_javascript');

        // $filename = BP . DS . 'skin' . DS . 'frontend' . DS . 'base' . DS . 'default' . DS . 'js' . DS
        //     . 'msrp.js';
        $filename = BP . DS . 'js' . DS . 'prototype' . DS
            . 'prototype.js';
        $contents = file_get_contents($filename);
        $size = strlen($contents);
        $isTest = Mage::getSingleton('adminhtml/session')->getCheckJsOnConfig();

        if ($isTest) {
            Varien_Profiler::enable();
        }
        foreach ($consts as $const) {

            try {
                $title = Mage::helper('TM_Pagespeed')->__(ucfirst($const));

                if ($isTest) {
                    Varien_Profiler::start($const);
                    $_contents = $model->compress($contents, $const);
                    Varien_Profiler::stop($const);
                    $_size = strlen($_contents);

                    // $title = str_pad($title, 16, ' ') . ' : ' . $_size . " : -"
                    //     . (100 - round(($_size / $size) * 100, 2)) . '% : '
                    //     . round(Varien_Profiler::fetch($const), 6);

                    $title = str_pad($title, 16, ' ') //. ' : ' . $_size . " : -"
                        . " ~compression "
                        . (100 - round(($_size / $size) * 100, 2)) . '% ('
                        . round(Varien_Profiler::fetch($const), 3). ' msec)';

                    Mage::getSingleton('adminhtml/session')->addSuccess(
                        Mage::helper('TM_Pagespeed')->__($title)
                    );
                }
                $result[$const] = $title;
            } catch (Exception $e) {
                Mage::logException($e);
                $logFile = Mage::getStoreConfig("dev/log/exception_file");
                Mage::getSingleton('adminhtml/session')->addNotice(
                    Mage::helper('TM_Pagespeed')->__(
                        "Compression method '%s' is not supported, look at the log file %s for more information", $title, $logFile
                    )
                );
            }
        }
        return $result;
    }

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        foreach ($this->_toOptionHash() as $key => $value) {
            $result[] = array('value' => $key, 'label' => $value);
        }
        return $result;
    }
}
