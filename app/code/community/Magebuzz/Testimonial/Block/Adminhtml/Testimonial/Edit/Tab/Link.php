<?php

class Magebuzz_Testimonial_Block_Adminhtml_Testimonial_Edit_Tab_Link extends  Varien_Data_Form_Element_Abstract
{
    protected $_element;

    public function getElementHtml()
    {
        $_product = Mage::getModel('catalog/product')->load($this->getVl());
        if ($_product->getId()) {
            $url =  '<a href="/ru/' . $_product->getUrlPath() . '" target="_blank">'.$_product->getName().'</a><br/><span>'.$_product->getSku().'</span>';
        } else {
            $url =  '<small style="color: gray">Нет связаной книги (возможно вопрос задан не со страницы продукта)</small>';
        }

        return $url;
    }
}