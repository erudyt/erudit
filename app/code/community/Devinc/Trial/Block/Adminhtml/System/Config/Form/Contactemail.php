<?php
class Devinc_Trial_Block_Adminhtml_System_Config_Form_Contactemail extends Mage_Adminhtml_Block_System_Config_Form_Field
{    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $html = '';

        $html.= '<tr><td style="position:absolute; padding:5px;">
                    Do you have issues, suggestions or a burning question?<br/>
                    Feel free to contact us at <b>info@developers-inc.com</b> and we will respond to you ASAP.<br/><br/>
                    If you\'re using a custom theme, some design/javascript issues might occur. If so, please let us know and we will fix them free of charge.<br/><br/>
                    Hope you enjoy our extension!<br/><br/>
                    Kind regards,<br/>
                    The Developers-inc team.
                </td><td class="value" style="height:158px;">&nbsp;</td></tr>'; 
        $html.= "<script type=\"text/javascript\">           
                    setTimeout(function() {
                        if ($$('.section-config')[1] && !$$('.section-config')[1].hasClassName('active')) {
                            Fieldset.toggleCollapse('ajaxcart_contact', '". $this->getUrl('*/*/state') . "'); 
                        }
                    },100);
                </script>
                <style>
                    input.validation-failed, textarea.validation-failed { background:#fef0ed !important; border:1px dashed #d6340e !important; }
                </style>";

        return $html;
    }     
      
    public function render(Varien_Data_Form_Element_Abstract $element)
    {     
        $html = '';   
        $html .= $this->_getElementHtml($element);
        
        return $html;
    }
}
