<?php

/**
 *  Jemoon_Htmlminify_Model_Observer
 *
 * @category   Jemoon
 * @package    Jemoon_Htmlminify
 * @author     Marcin Jędrzejewski <marcin.jedrzejewski85@gmail.com>
 * @copyright  Copyright (c) 2012 Marcin Jędrzejewski
 * @license    http://framework.zend.com/license   BSD License
 * @version    $id$
 * @since      0.1.0
 */
class Jemoon_Htmlminify_Model_Observer {

    public function Htmlminify($observer) {
        if (Mage::getStoreConfig('dev/html/minify_html_output')) {
            // Fetches the current event
            /*$event = $observer->getEvent();
            $controller = $event->getControllerAction();
            $allHtml = $controller->getResponse()->getBody();
            */
            
            //~
            $block     = $observer->getBlock();
	    $transport = $observer->getTransport();
	    $allHtml      = $transport->getHtml();
	    
            //~mage::log('controller: '.get_class($controller));

            // Trim each line
            $allHtml = preg_replace('/^\\s+|\\s+$/m', '', $allHtml);

            // Remove HTML comments
//~            $allHtml = preg_replace_callback('/<!--([\\s\\S]*?)-->/', array($this, '_commentCB'), $allHtml);
//	    $allHtml = preg_replace('#<!--(!-->).*-->#s', '', $allHtml);

	    //~ remove white-spaces
	    $search = array(
		'/\>[^\S ]+/s',  // strip whitespaces after tags, except space
		'/[^\S ]+\</s',  // strip whitespaces before tags, except space
		'/(\s)+/s'       // shorten multiple whitespace sequences
	    );
	    $replace = array(
		'>',
		'<',
		'\\1'
	    );
	    $allHtml = preg_replace($search, $replace, $allHtml);
	    //~
	    //$allHtml = preg_replace(array('/<!--(.*)-->/Uis', "/[[:blank:]]+/"), array('', ' '), str_replace(array("\n", "\r", "\t"), '', $allHtml));
	    

            // Remove ws around block/undisplayed elements
            $allHtml = preg_replace('/\\s+(<\\/?(?:area|base(?:font)?|blockquote|body'
                    . '|caption|center|cite|col(?:group)?|dd|dir|div|dl|dt|fieldset|form'
                    . '|frame(?:set)?|h[1-6]|head|hr|html|legend|li|link|map|menu|meta'
                    . '|ol|opt(?:group|ion)|p|param|t(?:able|body|head|d|h||r|foot|itle)'
                    . '|ul)\\b[^>]*>)/i', '$1', $allHtml);

            // Remove ws outside of all elements
            $allHtml = preg_replace_callback('/>([^<]+)</', array($this, '_outsideTagCB'), $allHtml);
            //~$controller->getResponse()->setBody($allHtml);
	    $transport->setHtml($allHtml);
	    
	    /*if ($this->getMaxMinifyStatus()) {
		    $transport->setHtml(Minify_HTMLMaxComp::minify($html, $this->minifyOptions));
	    } else {
		    $transport->setHtml(Minify_HTMLComp::minify($html, $this->minifyOptions));
	    }*/
        }
    }

    protected function _outsideTagCB($m) {
        return '>' . preg_replace('/^\\s+|\\s+$/u', ' ', $m[1]) . '<';
    }

    protected function _commentCB($m) {
        return (0 === strpos($m[1], '[') || false !== strpos($m[1], '<![')) ? $m[0] : '';
    }

}
