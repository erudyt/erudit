<?php

class Mage_Adminhtml_Block_Dashboard_Searches_Oneclickorder extends Mage_Adminhtml_Block_Dashboard_Grid
{
    protected $_collection;

    public function __construct()
    {
        parent::__construct();
        $this->setId('newQGrid');
    }

    protected function _prepareCollection()
    {
        /** @var $this->_collection Smasoft_Oneclickorder_Model_Resource_Order_Collection */
        $this->_collection = Mage::getModel('smasoft_oneclickorder/order')->getCollection();
        $this->_collection->getSelect()->joinLeft(
            array('country' => Mage::getModel('smasoft_oneclickorder/country')->getResource()
                ->getTable('smasoft_oneclickorder/country')),
            'country.country_code = main_table.country',
            array('country.country_code', 'country.phone_code')
        );
        $this->_collection->joinCustomerAttribute('firstname');
        $this->_collection->joinCustomerAttribute('lastname');
        $this->_collection->setOrder('create_date', 'DESC');
        $this->_collection->setPageSize(5);

        $this->setCollection($this->_collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('customer_id', array(
            'header' => 'Имя',
            'index' => 'customer_id',
            'getter' => 'getGridCustomerName',
            'renderer' => 'smasoft_oneclickorder/adminhtml_orders_grid_renderer_customer',
        ));
        $this->addColumn('phone', array(
            'header'    => 'Телефон',
            'sortable'  => false,
            'index'     => 'phone',
            'getter' => 'getFullPhoneNumber',
            'filter_index' => 'main_table.phone',
//            'renderer'  => 'adminhtml/dashboard_searches_renderer_searchquery',
        ));

//        $this->addColumn('email', array(
//            'header'    => 'Электронная почта (email)',
//            'sortable'  => false,
//            'index'     => 'email',
//            'type'      => 'text'
//        ));
//
//        $this->addColumn('testimonial', array(
//            'header'    => 'Вопрос',
//            'sortable'  => false,
//            'index'     => 'testimonial',
//            'renderer'  => 'adminhtml/dashboard_searches_renderer_truncate',
//        ));
//
        $this->addColumn('create_date', array(
            'header' => 'Создан',
            'index' => 'create_date',
            'filter_index' => 'create_date',
            'type' => 'datetime',
        ));

        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/oneclickorder/view', array('id'=>$row->getId()));
    }
}
