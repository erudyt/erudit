<?php

class Mage_Adminhtml_Block_Dashboard_Searches_CallBack extends Mage_Adminhtml_Block_Dashboard_Grid
{
    protected $_collection;

    public function __construct()
    {
        parent::__construct();
        $this->setId('newQGrid');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('webforms/results')->getCollection();

        $collection->getSelect()->joinLeft(
            array('nm' => $collection->getTable('webforms/results_values')),
            'nm.result_id = main_table.id and nm.field_id = 1',
            array('name' => 'nm.value')
        );
        $collection->getSelect()->joinLeft(
            array('ph' => $collection->getTable('webforms/results_values')),
            'ph.result_id = main_table.id and ph.field_id = 9',
            array('phone' => 'ph.value')
        );
        $collection->setOrder('created_time', 'DESC');
        $collection->addFilter('webform_id', 1);
        $collection->setPageSize(5);
        $this->_collection = $collection;

        $this->setCollection($this->_collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('name', array(
            'header'    => 'Имя',
            'sortable'  => false,
            'index'     => 'name',
            'type'     => 'text',
        ));

        $this->addColumn('phone', array(
            'header'    => 'Телефон',
            'sortable'  => false,
            'index'     => 'phone',
            'type'     => 'text',
        ));

        $this->addColumn('created_time', array(
            'header'    => 'Создан',
            'sortable'  => false,
            'index'     => 'created_time',
            'type'      => 'datetime'
        ));

        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('webforms/adminhtml_results/index', array('webform_id'=>1));
    }
}
