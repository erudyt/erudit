<?php
/**
 * zeonsolutions inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.zeonsolutions.com/shop/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento ENTERPRISE edition
 * =================================================================
 * zeonsolutions does not guarantee correct work of this extension
 * on any other Magento edition except Magento ENTERPRISE edition.
 * zeonsolutions does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Zeon
 * @package    Zeon_Manufacturer
 * @version    0.0.1
 * @copyright  @copyright Copyright (c) 2013 zeonsolutions.Inc. (http://www.zeonsolutions.com)
 * @license    http://www.zeonsolutions.com/shop/license-enterprise.txt
 */

class Zeon_Manufacturer_Block_List extends Mage_Core_Block_Template
{
    protected $_manufacturersCollection;

    /**
     * Retrieve Manufacturers collection
     *
     * @return Zeon_Manufacturer_Model_Resource_Manufacturer_Collection
     */
    protected function _getManufacturersCollection()
    {
        if (is_null($this->_manufacturersCollection)) {
            $this->_manufacturersCollection = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
                                    ->distinct(true)
                                    ->addStoreFilter(Mage::app()->getStore()->getId())
                                    ->addFieldToFilter('status', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
                                    
									;
        $this->_getManufacturersCollection()->getSelect()->order('name ASC');
		
		
		}
        return $this->_manufacturersCollection;
    }



 protected function getManufacturersAlpha($a)
    {
       
         /*$collection=Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
                                    ->distinct(true)
                                    ->addStoreFilter(Mage::app()->getStore()->getId())
                                    ->addFieldToFilter('status', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
                                     ->addFieldToFilter('name',array('like' => $a.'%'));*/

     //$collection->getSelect()->order('name ASC');*/
		
		$storeId = Mage::app()->getStore()->getStoreId();
          
		  if ($storeId == 1) {
		    $collection = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
			
			->addStoreFilter($storeId)
			->addFieldToFilter('status', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
			->addFieldToFilter('name',array('like' => $a.'%'))
			->setOrder('name','ASC')
			 ->distinct(true)
			 ->load();
			// $collection->getSelect()->order('name','ASC');
			
					//print_r  ($collection); die;
					$list_code= array();
					foreach ( $collection as $item)
					{ $list_code[]=array('name'=>$item->getName(),'links'=>$item->getIdentifier());
					
					//echo $item->getName();
					//echo "<br>";
						}
						$avtor_=$list_code;
		  }
			
			if ($storeId == 2) {
		    $collection = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
			->addStoreFilter($storeId)
			->addFieldToFilter('status', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
			->addFieldToFilter('name_ukr',array('like' => $a.'%'))
			->setOrder('name','ASC')
			 ->distinct(true)
			 ->load();;
					//print_r  ($collection); die;
		  }
			
			
			
		/*	echo  $collection->getSelect();
           $tableNameEAOV = Mage::getModel('core/resource')->getTableName('eav_attribute_option_value');
           $tableNameEAO = Mage::getModel('core/resource')->getTableName('eav_attribute_option');

         $collection->getSelect()->distinct(true)->join(
               array('eaov' => $tableNameEAOV), 
               'main_table.manufacturer = eaov.option_id', 
			 
               array('manufacturer_name'=>'value')
           )
           ->join(array('eao' => $tableNameEAO), 'eao.option_id = eaov.option_id',  array())->where('eaov.store_id IN (?)', $storeId)->group('manufacturer_name'); */ 
		//die;
       return   $collection ;
    }



	 protected function getManufacturersCountProduct($id)
    { 

	$collection = Mage::getModel('catalog/product')->getCollection();
         $collection->addAttributeToSelect('name');
		$collection->addAttributeToFilter('status', array('eq' => 1));
		 $collection->addAttributeToFilter('visibility', array('neq' => 1));
		 $collection->addAttributeToFilter('avtor', $id);
		// $collection->setOrder('price', 'ASC');
		
		$count_= count($collection);
		return $count_;
		
	
	}
    /**
     * Retrieve loaded Manufacturers collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getManufacturersCollection()
    {
        return $this->_getManufacturersCollection();
    }

    /**
     * Prepare global layout
     *
     * @return Zeon_Manufacturer_Block_List
     */
    protected function _prepareLayout()
    {
        $helper = Mage::helper('zeon_manufacturer');
        // show breadcrumbs
        if ($breadcrumbs = $this->getLayout()->getBlock('breadcrumbs')) {
            $breadcrumbs->addCrumb(
                'home', 
                array(
                    'label'=>$helper->__('Home'), 
                    'title'=>$helper->__('Go to Home Page'), 
                    'link'=>Mage::getBaseUrl()
                )
            );
           
        }
        $head = $this->getLayout()->getBlock('head');
        if ($head) {
            $head->setTitle($helper->getDefaultTitle());
            $head->setKeywords($helper->getDefaultMetaKeywords());
            $head->setDescription($helper->getDefaultMetaDescription());
        }

        return parent::_prepareLayout();
    }
}