<?php
/**
 * zeonsolutions inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.zeonsolutions.com/shop/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento ENTERPRISE edition
 * =================================================================
 * zeonsolutions does not guarantee correct work of this extension
 * on any other Magento edition except Magento ENTERPRISE edition.
 * zeonsolutions does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Zeon
 * @package    Zeon_Manufacturer
 * @version    0.0.1
 * @copyright  @copyright Copyright (c) 2013 zeonsolutions.Inc. (http://www.zeonsolutions.com)
 * @license    http://www.zeonsolutions.com/shop/license-enterprise.txt
 */

class Zeon_Manufacturer_Block_Left extends Mage_Core_Block_Template
{
    protected $_manufacturersCollection;

    /**
     * Retrieve Manufacturers collection
     *
     * @return Zeon_Manufacturer_Model_Resource_Manufacturer_Collection
     */
    protected function _getManufacturersCollection()
    {
        
									
			if (is_null($this->_manufacturersCollection)) {
           $storeId = Mage::app()->getStore()->getStoreId();
            $this->_manufacturersCollection = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')->addFieldToFilter('top', 1);;
			
           $tableNameEAOV = Mage::getModel('core/resource')->getTableName('eav_attribute_option_value');
           $tableNameEAO = Mage::getModel('core/resource')->getTableName('eav_attribute_option');

        $this->_manufacturersCollection->getSelect()->distinct()->join(
               array('eaov' => $tableNameEAOV), 
               'main_table.manufacturer = eaov.option_id', 
			 
               array('manufacturer_name'=>'value')
           )
           ->join(array('eao' => $tableNameEAO), 'eao.option_id = eaov.option_id',  array())->where('eaov.store_id IN (?)', $storeId)
		    ->order('manufacturer_name ASC')->group('manufacturer_name')
		  
		   ;
									
       
									
        }
        return $this->_manufacturersCollection;
    }

    /**
     * Retrieve loaded Manufacturers collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getManufacturersCollection()
    {
        return $this->_getManufacturersCollection();
    }
}