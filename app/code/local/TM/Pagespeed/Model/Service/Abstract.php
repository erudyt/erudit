<?php

abstract class TM_Pagespeed_Model_Service_Abstract
{
    // abstract public function saveOrigin();
    abstract public function smush($path);

    abstract public function check();

    abstract public function restore($source, $path);

    protected function _getArchiveFilename($filename)
    {
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        return str_replace(
            '.' . $ext, '.' . filemtime($filename) . '.' . $ext, $filename
            // '.' . $ext, '.' . md5($filename) . '.' . $ext, $filename
        );
    }
}