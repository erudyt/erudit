<?php
class TM_Pagespeed_Model_Observer
{
    const ENABLE_CONFIG          = 'tm_pagespeed/general/enabled';
    const LAZYLOAD_ENABLE_CONFIG = 'tm_pagespeed/general/lazyload_enabled';
    const SMUSHIT_ENABLE_CONFIG  = 'tm_pagespeed/smushit/enabled';
    const SMUSHIT_COUNT_CONFIG   = 'tm_pagespeed/smushit/count';
    const EXPIRES_ENABLE_CONFIG  = 'tm_pagespeed/expires/enabled';
    const EXPIRES_DAY_CONFIG     = 'tm_pagespeed/expires/day';
    const MINIFY_CONTENT_ENABLE_CONFIG = 'tm_pagespeed/minify_content/enabled';
    const MINIFY_CONTENT_TYPE_CONFIG   = 'tm_pagespeed/minify_content/type';

    /**
     *
     * @return boolean
     */
    protected function _isAjax()
    {
        return Mage::app()->getRequest()->isAjax();
    }

    protected function _isActual($config)
    {
        return ((bool) Mage::getStoreConfig(self::ENABLE_CONFIG))
            && !Mage::app()->getRequest()->isAjax()
            && !Mage::app()->getStore()->isAdmin()
            && ((bool) Mage::getStoreConfig($config))
        ;
    }

    protected function _isRootBlock($observer)
    {
        $block = $observer->getBlock();
        if (!$block) {
            return false;
        }
        $blockName = $block->getNameInLayout();
        if (empty($blockName) || $blockName !== 'root') {
            return false;
        }
        return true;
    }

    //https://github.com/aFarkas/lazysizes
    //https://github.com/aFarkas/respimage
    public function prepareLazyLoad($observer)
    {
        if (!$this->_isActual(self::LAZYLOAD_ENABLE_CONFIG)
            || !$this->_isRootBlock($observer)) {
            return;
        }
        Varien_Profiler::enable();
        $timerName = __METHOD__;
        Varien_Profiler::start($timerName);

        $transport = $observer->getTransport();
        $html = $transport->getHtml();

        $matches = array();
        preg_match_all( '/<img[\s\r\n]+.*?>/is', $html, $matches);

        $placeholder = '';//"data:image/gif;base64,R0lGODlhAQABAIAAAAAAAAAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==";
        $dom = new Zend_Dom_Query();
        foreach ($matches[0] as $_img) {
            $dom->setDocumentHtml($_img);
            $node = $dom->query('img')->current();

            $class = $node->getAttribute('class') . ' lazyload';
            $node->setAttribute('class', trim($class));

            foreach (array('src', 'srcset') as $attrName) {
                $attrValue = $node->getAttribute($attrName);
                if (!empty($attrValue)) {
                    $node->setAttribute('data-' . $attrName, $attrValue);
                    $node->setAttribute($attrName, $placeholder);
//                    $node->removeAttribute($attrName);
                }
            }
            //add alt for fix chrome width 0 (auto) bug
            $alt = $node->getAttribute('alt');
            if (empty($alt)) {
                $node->setAttribute('alt', 'lazyload');
            }

            $html = str_replace($_img, utf8_decode($node->C14N()), $html);
        }

        $transport->setHtml($html);

        Varien_Profiler::stop($timerName);
//        Zend_Debug::dump(Varien_Profiler::fetch($timerName));
    }

    public function minifyOutput($observer)
    {
        // return;
        if (!$this->_isActual(self::MINIFY_CONTENT_ENABLE_CONFIG)
            || !$this->_isRootBlock($observer)) {

            return;
        }

        $transport = $observer->getTransport();
        $html = $transport->getHtml();
        if (stripos($html, '<!DOCTYPE html') === false) {
            return;
        }

        Varien_Profiler::enable();
        $timerName = __METHOD__;
        Varien_Profiler::start($timerName);

        $type = Mage::getStoreConfig(self::MINIFY_CONTENT_TYPE_CONFIG);
        // Zend_Debug::dump($type);
        switch ($type) {
            case TM_Pagespeed_Model_System_Config_Source_Minify_Html_Type::LITE_TYPE:
                $html = TM_Pagespeed_Model_Minify_Html::liteMin($html);
                break;

            case TM_Pagespeed_Model_System_Config_Source_Minify_Html_Type::DEFAULT_TYPE:
            default:
                $html = TM_Pagespeed_Model_Minify_Html::min($html);
                break;
        }

        $transport->setHtml($html);

        Varien_Profiler::stop($timerName);
//        Zend_Debug::dump(Varien_Profiler::fetch($timerName));
    }

    public function setPageExpires($observer)
    {
        if (!$this->_isActual(self::EXPIRES_ENABLE_CONFIG)) {
            return;
        }

        /** @var $response Mage_Core_Controller_Response_Http */
        $response = $observer->getResponse();
        $day = (int) Mage::getStoreConfig(self::EXPIRES_DAY_CONFIG); //12
        $accessPlus = 60 * 60 * 24 * $day;
//        $response->setHeader('Expires', gmdate('D, d M Y H:i:s \G\M\T', time() + (3600 * 3600 * 20)));
        $response->setRawHeader('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + $accessPlus));
    }

    /**
     *
     * @param Mage_Cron_Model_Schedule $schedule
     * @return TM_Pagespeed_Model_Observer
     */
    public function smush($schedule)
    {
        $isEnabled = ((bool) Mage::getStoreConfig(self::SMUSHIT_ENABLE_CONFIG))
            && ((bool) Mage::getStoreConfig(self::ENABLE_CONFIG));
        if (!$isEnabled) {
            return;
        }
        $model = Mage::getModel("TM_Pagespeed/smushit");
        $model->check();
        $paths = $model->getPaths();
        $count = (int) Mage::getStoreConfig(self::SMUSHIT_COUNT_CONFIG);
        $model->smush($paths, $count);

        return $this;
    }
}