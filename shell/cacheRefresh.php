require_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR. 'app' . DIRECTORY_SEPARATOR . 'Mage.php';
 
$app = Mage::app('admin', 'store');
if($app->cleanCache()) {
    Mage::log("Successfully was cleared the cache.");
    //echo 'Successfully was cleared the cache.';
} else { 
    Mage::log("An error occurred while flushing the cache.");
    //echo 'An error occurred while flushing the cache.';
}

