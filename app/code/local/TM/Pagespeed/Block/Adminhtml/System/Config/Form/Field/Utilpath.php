<?php
class TM_Pagespeed_Block_Adminhtml_System_Config_Form_Field_Utilpath extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $_id = $element->getHtmlId();
        $value = Mage::getModel('TM_Pagespeed/Service_Util')
            ->getAutodetectedUtilPath();
        $_html = '<input type="hidden" id="' . $_id . '">';
        return $_html . $value;
    }
}
