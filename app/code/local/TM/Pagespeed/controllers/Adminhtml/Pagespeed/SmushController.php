<?php

class TM_Pagespeed_Adminhtml_Pagespeed_SmushController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        $this->loadLayout();
        $this->_setActiveMenu('pagespeed')
            ->_addBreadcrumb(
                Mage::helper('TM_Pagespeed')->__('pagespeed'),
                Mage::helper('TM_Pagespeed')->__('pagespeed')
            );

        return $this;
    }

    public function indexAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function imagesAction()
    {
        $this->_initAction();
        $this->renderLayout();
    }

    public function massOptimizeAction()
    {
        try {
            $paths = $this->getRequest()->getParam('paths');
            $model = Mage::getModel("TM_Pagespeed/smushit");
            $count = (int) Mage::getStoreConfig(
                TM_Pagespeed_Model_Observer::SMUSHIT_COUNT_CONFIG
            );
            $model->smush($paths, $count);
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('TM_Pagespeed')->__('%s images were successfully optimised <a href="%s">View Results</a>',
                    ((count($paths) > $count) ? $count : count($paths)),
                    $this->getUrl('*/*/index')
                )
            );
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('*/*/images');
    }

    public function smushAction()
    {
        try {
            Mage::getModel("TM_Pagespeed/observer")->smush(new Varien_Object());
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('TM_Pagespeed')->__('Records was successfully optimised')
            );
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::logException($e);
        }
        $this->_redirect('*/*/index');
    }

    public function deleteAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel("TM_Pagespeed/smushit");
        $model->load($id);
        $model->delete();
        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('TM_Pagespeed')->__('Record was successfully deleted')
        );
        $this->_redirect('*/*/index');
    }

    public function restoreAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel("TM_Pagespeed/smushit");
        $model->load($id);
        $model->restore();
        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('TM_Pagespeed')->__('Image was successfully restored')
        );
        $this->_redirect('*/*/index');
    }

    public function resmushAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel("TM_Pagespeed/smushit");
        $model->load($id);

        $model->restore();
        $model->resmush();

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('TM_Pagespeed')->__('Image was successfully re-optimized')
        );
        $this->_redirect('*/*/index');
    }

    public function menualsmushAction()
    {
        $id = $this->getRequest()->getParam('id');
        $model = Mage::getModel("TM_Pagespeed/smushit");
        $model->load($id);

        // $model->restore();
        $model->resmush();

        Mage::getSingleton('adminhtml/session')->addSuccess(
            Mage::helper('TM_Pagespeed')->__('Image was successfully optimized')
        );
        $this->_redirect('*/*/index');
    }

    /**
     * Check the permission to run it
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        $actionName = strtolower($this->getRequest()->getActionName());

        $path = 'templates_master/pagespeed/';
        switch ($actionName) {
            case 'images':
                $path .= 'images';
                break;
            default:
                $path .= 'smushit';
                break;
        }

        return Mage::getSingleton('admin/session')->isAllowed($path);
    }
}
