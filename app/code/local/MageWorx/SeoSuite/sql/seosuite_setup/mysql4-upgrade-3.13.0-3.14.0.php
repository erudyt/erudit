<?php
/**
 * MageWorx
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the MageWorx EULA that is bundled with
 * this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.mageworx.com/LICENSE-1.0.html
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade the extension
 * to newer versions in the future. If you wish to customize the extension
 * for your needs please refer to http://www.mageworx.com/ for more information
 *
 * @category   MageWorx
 * @package    MageWorx_SeoSuite
 * @copyright  Copyright (c) 2014 MageWorx (http://www.mageworx.com/)
 * @license    http://www.mageworx.com/LICENSE-1.0.html
 */
/**
 * SEO Suite extension
 *
 * @category   MageWorx
 * @package    MageWorx_SeoSuite
 * @author     MageWorx Dev Team
 */

$installer = $this;
$installer->startSetup();

$attribute = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, 'orig_name');

if($attribute->getId()){
    $attribute->setIsGlobal(Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE);
    $attribute->save();
}

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [orig_name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone. \r\n<p><font color = red>Note: You cannot use the [name] attribute in the product name template. Please use the [orig_name] instead.</font> \r\n<p><b>Example</b><p>[orig_name][ by {manufacturer|brand}][ of {color} color][ for {price}] <p>will be transformed into\r\n<p>HTC Touch Diamond by HTC of Black color for € 517.50\r\n<p>'
WHERE `template_code` = 'product_name'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [orig_name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone. \r\n<p><b>Example</b><p>[name][ by {manufacturer|brand}][ {color} color][ for {price}] <p>will be transformed into\r\n<p>htc-touch-diamond-by-htc-black-color-for-517-50\r\n<p>'
WHERE `template_code` = 'product_url'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone.\r\n<p>Additional variables available for Product Meta Title: [category], [categories], [store_name], [website_name]\r\n<p>\r\n<p>Please note that variables [category] and [categories] will be applied when the template is used dynamically only (see \"Enable Dynamic Meta Title for Product and Category Pages\").\r\n<p><b>Example</b>\r\n<p>[name][ by {manufacturer|brand}][ ({color} color)][ for {price}][ in {categories}] <p>will be transformed into\r\n<p>HTC Touch Diamond by HTC (Black color) for € 517.50 in Cell Phones - Electronics'
WHERE `template_code` = 'product_meta_title'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone.\r\n<p>Additional variables available for Product Meta Description: [category], [categories], [store_name], [website_name]\r\n<p>\r\n<p>Please note that variables [category] and [categories] will be applied when the template is used dynamically only (see \"Enable Dynamic Meta Description for Product and Category Pages\").\r\n<p><b>Example</b>\r\n<p>Buy [name][ by {manufacturer|brand}][ of {color} color][ for only {price}][ in {categories}] at[ {store_name},][ website_name]. [short_description] <p>will be transformed into\r\n<p>Buy HTC Touch Diamond by HTC of Black color for only € 517.50 in Cell Phones - Electronics at Digital Store, Digital-Store.com. HTC Touch Diamond signals a giant leap forward in combining hi-tech prowess with intuitive usability and exhilarating design'
WHERE `template_code` = 'product_meta_description'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone.\r\n<p>Additional variables available for Product Meta Keywords: [category], [categories], [store_name], [website_name]\r\n<p>\r\n<p>Please note that variables [category] and [categories] will be applied when the template is used dynamically only (see \"Enable Dynamic Meta Keywords for Product and Category Pages\").\r\n<p><b>Example</b>\r\n<p>[name][, {color} color][, {size} size][, category] <p>will be transformed into\r\n<p>CN Clogs Beach/Garden Clog, Blue color, 10 size, Shoes'
WHERE `template_code` = 'product_meta_keywords'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone.\r\n<p><b>Example</b>\r\n<p>Buy [name][ by {manufacturer|brand}][ of {color} color][ for only {price}.][ short_description] <p>will be transformed into\r\n<p>Buy HTC Touch Diamond by HTC of Black color for only € 517.50. HTC Touch Diamond signals a giant leap forward in combining hi-tech prowess with intuitive usability and exhilarating design'
WHERE `template_code` = 'product_description'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Template variables</b>\r\n<p>[attribute] — e.g. [name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone.\r\n<p><b>Example</b>\r\n<p>Buy [name][ by {manufacturer|brand}][ of {color} color][ for only {price}.][ short_description] <p>will be transformed into\r\n<p>Buy HTC Touch Diamond by HTC of Black color for only € 517.50. HTC Touch Diamond signals a giant leap forward in combining hi-tech prowess with intuitive usability and exhilarating design'
WHERE `template_code` = 'product_description'
");

$installer->run("
UPDATE `{$this->getTable('seosuite_template')}`
SET `comment` = '<p><p><b>Earlier input data for product image labels will be rewritten.</b>\r\n<p>
     <b>Template variables</b>\r\n<p>[attribute] — e.g. [name], [price], [manufacturer], [color] — will be replaced with the respective product attribute value or removed if value is not available\r\n<p>[attribute1|attribute2|...] — e.g. [manufacturer|brand] — if the first attribute value is not available for the product the second will be used and so on untill it finds a value\r\n<p>[prefix {attribute} suffix] or\r\n<p>[prefix {attribute1|attribute2|...} suffix] — e.g. [({color} color)] — if an attribute value is available it will be prepended with prefix and appended with suffix, either prefix or suffix can be used alone\r\n
     <p><b>Example</b>\r\n<p>[name][ - color is {color}] <p> will be transformed (if attribute <i>color</i> exist) into\r\n<p>BlackBerry 8100 Pearl - color is Silver.'
WHERE `template_code` = 'product_gallery'
");

$installer->endSetup();
?>
