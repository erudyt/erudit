<?php
class Devinc_Trial_IndexController extends Mage_Core_Controller_Front_Action 
{	     
	public function reinitAction()    
	{
		//additional disable paths
		$additionalPaths['groupdeals'] = array('groupdeals/facebook_connect/enabled');
		$additionalPaths['dailydeal'] = array('dailydeal/configuration/header_links');
		$additionalPaths['multipledeals'] = array('multipledeals/configuration/header_links');
		
		$module = $this->getRequest()->getParam('module', false);
		$storeIds = Mage::getModel('trial/trial')->getStoreIds();
		if ($module) {
			foreach ($storeIds as $storeId) {
				if (!in_array(base64_encode('enabled_'.$module), Mage::getModel('trial/trial')->isStoreValid($module, $storeId))) {
					Mage::getModel('trial/trial')->disableExtensionFrontendStore($module, $additionalPaths[$module], $storeId);
				}
			}
		}
		
		$this->_redirect('');
	}

}
