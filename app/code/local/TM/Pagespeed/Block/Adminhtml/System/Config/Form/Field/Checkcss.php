<?php

class TM_Pagespeed_Block_Adminhtml_System_Config_Form_Field_Checkcss
    extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $isTest = Mage::getSingleton('adminhtml/session')->getCheckCssOnConfig();
        if ($isTest) {
            Mage::getSingleton('adminhtml/session')->setCheckCssOnConfig(false);
            return '';
        }
        $id = '_' . strtolower(get_class($this));//md5(__METHOD__);
        $url  = $this->getUrl('adminhtml/pagespeed_test/test', array(
            'var_name' => 'check_css_on_config'
        ));
        $html = '<tr><td colspan="100">'
            . '<button onclick="check' . $id . '();" class="scalable save" type="button"><span><span><span>'
            . $this->__('Test compression')
            . '</span></span></span></button>'
            // . '<p class="note"><span>' . $this->__('Enable checking js compressing') . '</span></p>'
            . '<script type="text/javascript">'
            . ' function check' . $id . '()'
            . ' {
                    var url = \'' . $url . '\';
                    window.location = url;
                    return false;
               }'
            . '</script>'
            . '</br></td></tr>';
        return $html;
    }
}
