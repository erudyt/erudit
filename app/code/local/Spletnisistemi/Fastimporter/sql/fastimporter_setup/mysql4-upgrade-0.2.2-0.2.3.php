<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();

$setup->addAttribute('catalog_product', 'zero_qty_count', array(
  'backend_type'   => 'int',
  'type'           => 'int',
  'visible'        => 0,
  'required'       => 0,
  'user_defined'   => 1,
  'global'         => 1,
  'default'        => 0,
));


$table = $installer->getTable('fastimporter');

$installer->run("
        ALTER TABLE $table ADD COLUMN `disabled` VARCHAR(255) NOT NULL;
        ALTER TABLE $table ADD COLUMN `zeroqty` INTEGER;
");

$installer->endSetup();
