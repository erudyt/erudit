<?php

class Devinc_License_Model_System_Config_Backend_License extends Mage_Core_Model_Config_Data
{    
    public function _beforeSave()
    {        
    	$path = $this->getPath();
    	$pathParts = explode('/', $path);
    	$module = $pathParts[1];
    	
        $domains = Mage::getModel('license/license')->getDomains($module);		
        foreach ($domains as $domain) {
        	$origValues[$domain] = '';
        }           
		
		$value = trim(base64_decode(Mage::getStoreConfig($path, 0)));
		if ($value!='') {
			if (strpos($value, ',')) {
				$origValueGroups = explode(',',$value);			       		
        		foreach ($origValueGroups as $group) {
	    		    $key_value = explode('::',$group);
    			    $origValues[$key_value[0]] = $key_value[1];
        		}
			} else {
				$key_value = explode('::',$value);
				$origValues[$key_value[0]] = $key_value[1];
			}
        }
        		
        $valueArray = array(); 
        for ($i=0; $i<count($domains); $i++){
    		$ch = curl_init();
			$data = array('license_code' => $this->getValue($i), 'domain' => $domains[$i], 'module' => $module);       	
			
			curl_setopt($ch, CURLOPT_URL, 'http://www.developers-inc.com/license/index/register/');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		
			$result = curl_exec($ch);
			curl_close($ch);
			
			$error = 0;
        	$message = null;
        	if ($result!='') {
        		$resultElements = explode(';;', $result);
        		foreach ($resultElements as $element) {
        			if (strpos($element, '=')) {
        				list($key, $value) = explode('=', $element);
        				if ($key=='error') {
        					$error = $value;
        				}
        				
        				if ($key=='message') {
        					$message = $value;
        				}
        			}
        		}
        	}
        	
			if ($error==1) {
				Mage::getSingleton('core/session')->addError($message);	
				$valueArray[] = $domains[$i].'::'.$origValues[$domains[$i]];			
			} else if ($error==2) {				
				throw new Mage_Core_Exception($message);
			} else if (isset($message)) {
				Mage::getSingleton('core/session')->addSuccess($message);
			}
			
			if ($this->getValue($i)!='' && $error!=1) {
				$valueArray[] = $domains[$i].'::'.$this->getValue($i);
			}
		}
		
		$values = base64_encode(implode(',', $valueArray));
		$this->setValue($values);
    }
    
}
