<?php
class Devinc_Trial_Model_Module extends Devinc_Trial_Model_Trial
{  	
	protected function _trialModuleIntact() {
		$intact = 'intact';

		$fileNames = array(
			'Block'.DS.'Adminhtml'.DS.'System'.DS.'Config'.DS.'Form'.DS.'Activate.php', 			
			'Block'.DS.'Adminhtml'.DS.'System'.DS.'Config'.DS.'Form'.DS.'Contactemail.php', 	
			'controllers'.DS.'IndexController.php', 
			'etc'.DS.'config.xml', 
			'etc'.DS.'system.xml', 
			'Helper'.DS.'Data.php', 
			'Model'.DS.'Module.php', 
			'Model'.DS.'Trial.php', 
			'Model'.DS.'System'.DS.'Config'.DS.'Backend'.DS.'Email.php',
			'Model'.DS.'System'.DS.'Config'.DS.'Backend'.DS.'Trial.php'
		);
		
		foreach ($fileNames as $fileName) {
			$file = Mage::getBaseDir('code').DS.'community'.DS.'Devinc'.DS.'Trial'.DS.$fileName;
			if (!file_exists($file)) {
		    	$intact = 'not_intact';
		    	break;
		    }
		}

		if (!in_array(Mage::getModel('trial/module')->hookToControllerActionPreDispatch('verify'), array(base64_encode('is_valid')))) {
		    $intact = 'not_intact';			
		}

 		$file = Mage::getBaseDir().DS.'app'.DS.'etc'.DS.'modules'.DS.'Devinc_Trial.xml';
        $trialEnabled = false;
        if (file_exists($file)) {
            $xml = simplexml_load_file($file);
            if ($xml->modules->Devinc_Trial->active=='true') {
                $trialEnabled = true;
            }
        }
		$isModuleEnabled = Mage::getStoreConfig('advanced/modules_disable_output/Devinc_Trial');
		if ($isModuleEnabled!=0 || !$trialEnabled) {
		    $intact = 'not_intact';			
		}
		
		$xmlFile = Mage::getBaseDir('code').DS.'community'.DS.'Devinc'.DS.'Trial'.DS.'etc'.DS.'config.xml';
		if (file_exists($xmlFile)) {
			$xml = simplexml_load_file($xmlFile); 
    		$version = $xml->modules->Devinc_Trial->version;		
    		$adminxml = $xml->adminhtml->layout->updates->trial->file;	
    		$use = $xml->frontend->routers->trial->use;			
    		$module = $xml->frontend->routers->trial->args->module;			
    		$frontName = $xml->frontend->routers->trial->args->frontName;				
    		$class[] = $xml->global->events->controller_action_predispatch->observers->trial_controller_action_before->class;			
    		$method[] = $xml->global->events->controller_action_predispatch->observers->trial_controller_action_before->method;				
    		$models = $xml->global->models->trial->class;	
    		$resources[] = $xml->global->resources->trial_setup->setup->module;    		
    		$resources[] = $xml->global->resources->trial_setup->connection->use;
    		$blocks = $xml->global->blocks->trial->class;	
    		$helpers = $xml->global->helpers->trial->class;	
			if ($version!='0.1.0' || $adminxml!='trial.xml' || $use!='standard' || $module!='Devinc_Trial' || $frontName!='trial' || $class[0]!='trial/module' || $method[0]!='hookToControllerActionPreDispatch' || $models!='Devinc_Trial_Model' || $blocks!='Devinc_Trial_Block' || $helpers!='Devinc_Trial_Helper' || $resources[0]!='Devinc_Trial' || $resources[1]!='core_setup') {
				$intact = 'not_intact';
			}
		} else {
			$intact = 'not_intact';
		}
		
		return array(base64_encode($intact));
	}

	public function hookToControllerActionPreDispatch($observer)
    {
    	if (is_string($observer) && $observer=='verify') {
    		return base64_encode('is_valid');
    	}
		$modules = array('ajaxcart');
		//additional disable paths
		$additionalPaths['ajaxcart'] = array();

		//relogin on first install
 		$resource = Mage::getSingleton('core/resource');
	    $connection = $resource->getConnection('core_read');

	    $select = $connection->select()
	    ->from($resource->getTableName('core_config_data'))
	    ->where('scope = ?', 'default')
	    ->where('scope_id = ?', 0)
	    ->where('path = ?', 'devinc/install/relogin');

		$rows = $connection->fetchAll($select); 
		
		if (count($rows)>0) {
			$relogin = trim($rows[0]['value']);
			if ($relogin==1) {
				Mage::getModel('core/config')->saveConfig('devinc/install/relogin', 0, 'default', 0);		
				$adminSession = Mage::getSingleton('admin/session');
				// $adminSession->unsetAll();
				$adminSession->getCookie()->delete($adminSession->getSessionName());	
			}
		}   
		
		$actionName = $observer->getEvent()->getControllerAction()->getFullActionName();
		$controller = $observer->getControllerAction();
		$request = $controller->getRequest();
		$params = $request->getParams(); 
		if (!isset($params['store'])) $params['store'] = false;
		if (!isset($params['website'])) $params['website'] = false;

        if ($actionName == 'adminhtml_system_config_edit' && isset($params['section']) && in_array($params['section'], $modules) && !in_array(base64_encode('enabled_'.$params['section']), Mage::getModel('trial/trial')->_isValid($params['section']))) {   
        	Mage::getModel('trial/trial')->disableExtension($params['section'], $additionalPaths[$params['section']]);  
			Mage::getSingleton('core/session')->addNotice("Please start a trial to activate the extension. If you have any questions, feel free to contact us at info@developers-inc.com");
        } else if ($actionName == 'adminhtml_system_config_save' && in_array($params['section'], $modules)) {  
        	$domain = '';
        	if ($code = $params['store']) {
        		$storeId = Mage::app()->getStore($code)->getId(); 
				if (!in_array(base64_encode('enabled_'.$params['section']), Mage::getModel('trial/trial')->isStoreValid($params['section'], $storeId))) {
					Mage::getModel('trial/trial')->disableExtensionStore($params['section'], $additionalPaths[$params['section']], $storeId); 
					$message = "this Store View's";
					$domain = Mage::getModel('trial/trial')->getDomain($storeId);
        		}
        	} else if ($code = $params['website']) {   
        		$website = Mage::getModel('core/website')->load($code, 'code');				
				if ($websiteId = $website->getId()) {	
				    if (!in_array(base64_encode('enabled_'.$params['section']), Mage::getModel('trial/trial')->isStoreValid($params['section'], 0, $websiteId))) {
						Mage::getModel('trial/trial')->disableExtensionWebsite($params['section'], $additionalPaths[$params['section']], $websiteId); 
						$message = "this Website's";
						$domain = Mage::getModel('trial/trial')->getDomain(0, $websiteId);
        			}
				}    
        	} else {
        		$storeId = 0; 
				if (!in_array(base64_encode('enabled_'.$params['section']), Mage::getModel('trial/trial')->isStoreValid($params['section'], $storeId))) {
					Mage::getModel('trial/trial')->disableExtensionStore($params['section'], $additionalPaths[$params['section']], $storeId); 
					$message = 'the Default Config';
					$domain = Mage::getModel('trial/trial')->getDomain($storeId);
        		}
        	}
        	
        	if ($domain!='' && !isset($params['activate'])) {
        		unset($params['key']);
        		unset($params['form_key']);
            	Mage::app()->getResponse()->setRedirect(Mage::helper("adminhtml")->getUrl('adminhtml/system_config/edit', $params));
           		$controller->setFlag('', 'no-dispatch', true);
            	
				Mage::getSingleton('core/session')->addError("Your configuration wasn't saved because the extension isn't registered for ".$message." domain (".$domain."). If you have any questions, feel free to contact us at info@developers-inc.com");
			}			
		} else if (in_array($request->getModuleName(), $modules)) {
			$storeId = Mage::app()->getStore()->getId(); 
		
			if (!in_array(base64_encode('intact'), $this->_trialModuleIntact())) {
			    Mage::getSingleton('core/session')->addError("Your extension will not function properly because the trial module is not intact. Please make sure you copied all it's files to your FTP or contact us at info@developers-inc.com");
				if ($storeId!=0) {
					Mage::app()->getResponse()->setRedirect(Mage::getUrl('no-route'));
           			$controller->setFlag('', 'no-dispatch', true);
				}
			}
			
			// frontend
			if ($storeId!=0 && Mage::getModel('trial/trial')->_requiresVerification($storeId, $request->getModuleName()) && !in_array(base64_encode('enabled_'.$request->getModuleName()), Mage::getModel('trial/trial')->isStoreValid($request->getModuleName(), $storeId))) {
				Mage::getModel('trial/trial')->disableExtensionFrontendStore($request->getModuleName(), $additionalPaths[$request->getModuleName()], $storeId);
				Mage::app()->getResponse()->setRedirect(Mage::getUrl('no-route'));
			}
			
			// admin
			$frontendModuleName = str_replace('admin','', $request->getModuleName());
			if ($storeId==0 && Mage::getModel('trial/trial')->_requiresVerification($storeId, $request->getModuleName()) && !in_array(base64_encode('enabled_'.$frontendModuleName), Mage::getModel('trial/trial')->_isValid($frontendModuleName))) {
        		Mage::getModel('trial/trial')->disableExtension($frontendModuleName, $additionalPaths[$frontendModuleName]);
        		Mage::getSingleton('core/session')->addNotice("The extension isn't activated. Please start a trial to activate the extension. If you have any questions, feel free to contact us at info@developers-inc.com");
			}
		}
    }
}
