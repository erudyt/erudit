<?php

class Devinc_Trial_Model_System_Config_Backend_Trial extends Mage_Core_Model_Config_Data
{    
    public function _beforeSave()
    {        
    	$path = $this->getPath();
    	$pathParts = explode('/', $path);
    	$module = $pathParts[0];
    	$email = Mage::registry('customer_email');
		
		if ($email && $email!='') {    	
	    	$fieldValue = $this->getValue();
			if ($fieldValue!='' && $email!='') {
				if (strpos($fieldValue, ',')) {
		        	$domains = explode(',', $this->getValue());	
				} else {
		        	$domains[0] = $this->getValue();	
				}	  
		        		
		        for ($i=0; $i<count($domains); $i++){
		    		$ch = curl_init();
					$data = array('domain' => $domains[$i], 'module' => $module, 'email' => $email, 'full_domain' => "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");       	
					
					curl_setopt($ch, CURLOPT_URL, 'http://www.developers-inc.com/license/trial/register/');
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				
					$result = curl_exec($ch);
					curl_close($ch);
					
					$error = 0;
		        	$message = null;
		        	if ($result!='') {
		        		$resultElements = explode(';;', $result);
		        		foreach ($resultElements as $element) {
		        			if (strpos($element, '=')) {
		        				list($key, $value) = explode('=', $element);
		        				if ($key=='error') {
		        					$error = $value;
		        				}
		        				
		        				if ($key=='message') {
		        					$message = $value;
		        				}
		        			}
		        		}
		        	}
		        	
					if ($error==1) {
						Mage::getSingleton('core/session')->addError($message);	
						$originalValue = trim(base64_decode(Mage::getStoreConfig($path, 0)));
						$fieldValue = $originalValue;			
					} else if (isset($message)) {
						Mage::getSingleton('core/session')->addSuccess($message);
					}
				}
	        }

	        $encryptedValue = base64_encode($fieldValue);
	        $this->setValue($encryptedValue);
	    }
    }    
}
