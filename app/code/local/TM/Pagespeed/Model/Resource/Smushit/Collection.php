<?php
class TM_Pagespeed_Model_Resource_Smushit_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('TM_Pagespeed/smushit');
    }
}