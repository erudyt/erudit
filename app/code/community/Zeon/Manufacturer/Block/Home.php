<?php
/**
 * zeonsolutions inc.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.zeonsolutions.com/shop/license-enterprise.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * This package designed for Magento ENTERPRISE edition
 * =================================================================
 * zeonsolutions does not guarantee correct work of this extension
 * on any other Magento edition except Magento ENTERPRISE edition.
 * zeonsolutions does not provide extension support in case of
 * incorrect edition usage.
 * =================================================================
 *
 * @category   Zeon
 * @package    Zeon_Manufacturer
 * @version    0.0.1
 * @copyright  @copyright Copyright (c) 2013 zeonsolutions.Inc. (http://www.zeonsolutions.com)
 * @license    http://www.zeonsolutions.com/shop/license-enterprise.txt
 */

class Zeon_Manufacturer_Block_Home extends Mage_Core_Block_Template
{
    protected $_manufacturersCollection;

    /**
     * Retrieve Manufacturers collection
     *
     * @return Zeon_Manufacturer_Model_Resource_Manufacturer_Collection
     */
    protected function _getManufacturersCollection()
    {
        if (is_null($this->_manufacturersCollection)) {
            $this->_manufacturersCollection = Mage::getResourceModel('zeon_manufacturer/manufacturer_collection')
                            ->distinct(true)
                            ->addStoreFilter(Mage::app()->getStore()->getId())
                            ->addFieldToFilter('status', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
							->addFieldToFilter('top', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
                            ->addFieldToFilter('is_display_home', Zeon_Manufacturer_Model_Status::STATUS_ENABLED)
                            ->addOrder('sort_order', 'asc')
							->setPageSize(1)->setCurPage(1)->load();
        }
        return $this->_manufacturersCollection;
    }

    /**
     * Retrieve loaded Manufacturers collection
     *
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    public function getManufacturersCollection()
    {
        return $this->_getManufacturersCollection();
    }
	
	public function getBestsellingProduct($id_manuf)
{  


    // number of products to display
    $productCount =4;
     
    // store ID
    $storeId    = Mage::app()->getStore()->getId();
 
    // get today and last 30 days time
    $today = time();
    $last = $today - (60*60*24*30);
 
    $from = date("Y-m-d", $last);
    $to = date("Y-m-d", $today);
     
    // get most viewed products for current category
    $products = Mage::getResourceModel('reports/product_collection')
	
                    
					->addAttributeToSelect(array('name', 'price', 'small_image', 'avtor')) 
					
                    ->addOrderedQty($from, $to)
                    ->setStoreId($storeId)
                    ->addStoreFilter($storeId)                  
                    ->setOrder('ordered_qty', 'desc')
                    ->setPageSize($productCount);
  // $products->getSelect()->joinInner(array('e2' => 'catalog_product_flat_'.$storeId), 'e2.entity_id = e.entity_id'); 
   $products->addAttributeToFilter('avtor',array('eq'=> $id_manuf));
    Mage::getSingleton('catalog/product_status')->addVisibleFilterToCollection($products);
    Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($products);
     
//echo $products->getSelect();
   // echo count($products);	
	if (count($products)==0){
		
		$products_ = Mage::getResourceModel('reports/product_collection')->addAttributeToSelect('*')
		->setStoreId($storeId)
		->addStoreFilter($storeId)
		->addViewsCount()
		->setPageSize($productCount); 
		 $products_->addAttributeToFilter('avtor',array('eq'=> $id_manuf));
		Mage::getSingleton('catalog/product_status') ->addVisibleFilterToCollection($products_); 
Mage::getSingleton('catalog/product_visibility') ->addVisibleInCatalogFilterToCollection($products_);
		//echo $products_->getSelect();
		return $products_;
		}
	else
	
	{
		return $products;
		}
	
	
	
}

	
}