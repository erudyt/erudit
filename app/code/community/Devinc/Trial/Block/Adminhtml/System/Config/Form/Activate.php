<?php
class Devinc_Trial_Block_Adminhtml_System_Config_Form_Activate extends Mage_Adminhtml_Block_System_Config_Form_Field
{    
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $html = '';
        $isActivated = 0;
        $module = $this->getRequest()->getParam('section', 'ajaxcart');

        if ($this->getRequest()->getParam('website', false) || $this->getRequest()->getParam('store', false)) {   
            $html.= '<tr><td style="position:absolute; padding:5px;">The trials can only be entered for the Default Configuration Scope. Please change the Current Configuration Scope from the top left dropdown.</td><td class="value">&nbsp;</td></tr>'; 
            
            $domains = Mage::getModel('trial/trial')->getDomains($module);

            for ($i=0; $i<count($domains); $i++){  
                $daysLeft = Mage::getModel('trial/trial')->getDaysLeft($domains[$i]);
                if ($daysLeft!=-1 && $daysLeft!=0) {
                    $isActivated = 1;
                }     
            }
        } else {
            $html.= '<tr><td style="position:absolute; padding:5px;">Please check the domains for which you want to start your 30 days Free Trial of Ajax Cart.</td><td class="value">&nbsp;</td></tr>'; 
            
            $domains = Mage::getModel('trial/trial')->getDomains($module);
            $activeDomains = trim(base64_decode($element->getValue()));

            for ($i=0; $i<count($domains); $i++){  
                $remainingHTML = '<input class="domains" id="domain'.$i.'" name="domain'.'['.$i.']" value="'.$domains[$i].'" onclick="updateDomains()" type="checkbox" />';

                $daysLeft = Mage::getModel('trial/trial')->getDaysLeft($domains[$i]);
                if ((($daysLeft!=-1 && $daysLeft!=0) || $daysLeft=='unlimited') && $activeDomains!='') {
                    $remainingHTML = '<input class="domains" id="domain'.$i.'" name="domain'.'['.$i.']" value="'.$domains[$i].'" onclick="updateDomains()" type="hidden" />';
                    if ($daysLeft=='unlimited') {
                        $remainingHTML .= '<b>You can use the extension for an Unlimited number of days on localhost. Yey!</b>';
                    } else {
                        $remainingHTML .= '<b>'.$daysLeft.' day(s) left.</b>';
                    }
                    $isActivated = 1;
                } else if ($daysLeft==0 && $activeDomains!='') {
                    $remainingHTML = '<input class="domains" id="domain'.$i.'" name="domain'.'['.$i.']" value="'.$domains[$i].'" onclick="updateDomains()" type="hidden" />';
                    $remainingHTML .= '<b>Expired</b>';                    
                }

                $html.= '<tr>';   
                $html.= '<td class="label" style="width:auto;"><label for="domain'.$i.'" style="width:100%;">'.$domains[$i].'</label></td>';             
                $html.= '<td class="value">'.$remainingHTML.'</td>';  
                $html.= '</tr>';                    
            }
            $html.= '<tr>'; 
            $html.= '<td><input id="'.$element->getHtmlId().'" name="'.$element->getName().'" value="'.$activeDomains.'" type="hidden" /></td>';  
            $html.= '</tr>';

            // $activeDomainsNr = count(explode(',', $activeDomains));
            // $buyButtonStyle = 'style="margin-left:5px; position:absolute;"';
            $trialButton = '<button onclick="submitActivationForm()" class="scalable" type="button" title="Save Config" id="activate_ac_button"><span><span><span>Start Free Trial</span></span></span></button>';
            // if ($activeDomainsNr==count($domains)) {
            //     $trialButton = '';                
            //     $buyButtonStyle = '';                
            // }

            $buyButton = '';
            if ($isActivated==1) {
                $buyButton = '<button style="margin-left:5px; position:absolute;" onclick="window.open(\'http://www.developers-inc.com/ajax-cart.html\',\'_blank\');" class="scalable" type="button" title="Save Config" id="buy_ac"><span><span><span>Get Full Version</span></span></span></button>';
            }   

            $html.= '<tr><td style="padding:5px;position:relative;">'.$trialButton.$buyButton.'</td></tr>';
        }

        $html .= "<script type=\"text/javascript\">    
                    var isActivated = ".$isActivated.";
                    function updateDomains() {
                        var domains = [];
                        var i = 0;
                        $$('.domains').each(
                            function (index) {
                                if (index.checked) {
                                    domains[i] = index.value;
                                    i++;
                                } else if (index.type=='hidden') {
                                    domains[i] = index.value;
                                    i++;
                                }
                            }
                        );

                        $('".$element->getHtmlId()."').value = domains.join();
                    }
                    
                    function submitActivationForm() {
                        this.activationValidator  = new Validation('ajaxcart_activation');

                        if (this.activationValidator && this.activationValidator.validate()) {
                            var action = $('config_edit_form').getAttribute('action') + 'activate/1/';
                            $('config_edit_form').writeAttribute('action', action);

                            $$('#config_edit_form input').each(
                                function (index) {                                
                                    if (index.name!='groups[activation][fields][email][value]' && index.name!='groups[activation][fields][domains][value]' && index.name!='form_key') {
                                        Form.Element.disable(index);
                                    }
                                }
                            )

                            $$('#config_edit_form select').each(
                                function (index) {                                
                                    Form.Element.disable(index);
                                }
                            )

                            $$('#config_edit_form textarea').each(
                                function (index) {                                
                                    Form.Element.disable(index);
                                }
                            )
                            configForm.submit();
                        } 
                    }

                    Event.observe(window, 'load', function() { 
                        if ($('ajaxcart_activation_email')) {
                            $('ajaxcart_activation_email').addClassName('required-entry validate-email');
                            $$('#row_ajaxcart_activation_email label[for=\"ajaxcart_activation_email\"]')[0].update('Your Email <span class=\"required\">*</span>');
                        }

                        var i = 0;
                        $$('.section-config').each(
                            function (index) {                                
                                if (i!=0 && isActivated==0) {
                                    Element.remove(index);
                                } else if (i==0) {
                                    setTimeout(function() {
                                        if (!index.hasClassName('active')) {
                                            Fieldset.toggleCollapse('ajaxcart_activation', '". $this->getUrl('*/*/state') . "'); 
                                        }
                                    },100);
                                }
                                i++;
                            }
                        )

                        if (isActivated==0) {
                            $$('.form-buttons')[0].hide();
                        }
                    });
                </script>
                <style>
                    input.validation-failed, textarea.validation-failed { background:#fef0ed !important; border:1px dashed #d6340e !important; }
                </style>";

        return $html;
    }     
      
    public function render(Varien_Data_Form_Element_Abstract $element)
    {     
        $html = '';   
        $html .= $this->_getElementHtml($element);
        
        return $html;
    }
}
