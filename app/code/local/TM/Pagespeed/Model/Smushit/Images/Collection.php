<?php
class TM_Pagespeed_Model_Smushit_Images_Collection extends Varien_Data_Collection_Filesystem
{
    // protected $_dirsFirst = false;

    /**
     * Generate item row basing on the filename
     *
     * @param string $filename
     * @return array
     */
    protected function _generateRow($filename)
    {
        return array(
            'filename' => $filename,
            'basename' => basename($filename),
            'id'    => $filename,
            'path'  => $filename,
            'ctime' => filectime($filename),
            'mtime' => filemtime($filename),
            'size'  => filesize($filename),
            'perms' => substr(sprintf('%o', fileperms($filename)), -4)
        );
    }


    /**
     * Lauch data collecting
     *
     * @param bool $printQuery
     * @param bool $logQuery
     * @return Varien_Data_Collection_Filesystem
     */
    public function loadData($printQuery = false, $logQuery = false)
    {
        if ($this->isLoaded()) {
            return $this;
        }

        $model = Mage::getModel('TM_Pagespeed/smushit');
        $targetDirs = $model->getPaths();
        // Zend_Debug::dump($paths);


        // if (empty($this->_targetDirs)) {
        //     throw new Exception('Please specify at least one target directory.');
        // }

        $this->_collectedFiles = array();
        $this->_collectedDirs = array();
        // $this->_collectRecursive($this->_targetDirs);
        $this->_collectedFiles = $model->getFiles($targetDirs);
        $this->_generateAndFilterAndSort('_collectedFiles');
        // if ($this->_dirsFirst) {
        //     $this->_generateAndFilterAndSort('_collectedDirs');
        //     $this->_collectedFiles = array_merge($this->_collectedDirs, $this->_collectedFiles);
        // }

        // calculate totals
        $this->_totalRecords = count($this->_collectedFiles);
        $this->_setIsLoaded();

        // paginate and add items
        $from = ($this->getCurPage() - 1) * $this->getPageSize();
        $to = $from + $this->getPageSize() - 1;
        $isPaginated = $this->getPageSize() > 0;

        $cnt = 0;
        foreach ($this->_collectedFiles as $row) {
            $cnt++;
            if ($isPaginated && ($cnt < $from || $cnt > $to)) {
                continue;
            }
            $item = new $this->_itemObjectClass();
            $item->addData($row);

            $this->addItem($item);
            if (!$item->hasId()) {
                $item->setId($cnt);
            }
        }

        // $f = $this->getFirstItem();
        // Zend_Debug::dump($f->getData());

        return $this;
    }

    /**
     * Get all ids of collected items
     *
     * @return array
     */
    public function getAllIds()
    {
        return array_column($this->_collectedFiles, 'id');
        // return array_keys($this->_items);
    }

    /**
     * With specified collected items:
     *  - generate data
     *  - apply filters
     *  - sort
     *
     * @param string $attributeName '_collectedFiles' | '_collectedDirs'
     */
    private function _generateAndFilterAndSort($attributeName)
    {
        // generate custom data (as rows with columns) basing on the filenames
        foreach ($this->$attributeName as $key => $filename) {
            $this->{$attributeName}[$key] = $this->_generateRow($filename);
        }

        // Zend_Debug::dump($this->_filters);
        // apply filters on generated data
        if (!empty($this->_filters)) {
            foreach ($this->$attributeName as $key => $row) {
                if (!$this->_filterRow($row)) {
                    unset($this->{$attributeName}[$key]);
                }
            }
        }

        // sort (keys are lost!)
        if (!empty($this->_orders)) {
            usort($this->$attributeName, array($this, '_usort'));
        }
    }

    /**
     * Callback method for 'like' fancy filter
     *
     * @param string $field
     * @param mixed $filterValue
     * @param array $row
     * @return bool
     * @see addFieldToFilter()
     * @see addCallbackFilter()
     */
    public function filterCallbackLike($field, $filterValue, $row)
    {
        $filterValue = trim($filterValue, "'");
        $filterValueRegex = str_replace('%', '(.*?)', preg_quote($filterValue, '/'));
        // Zend_Debug::dump("/^{$filterValueRegex}$/i");
        return (bool)preg_match("/^{$filterValueRegex}$/i", $row[$field]);
    }
}