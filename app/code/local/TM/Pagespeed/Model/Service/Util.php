<?php
class TM_Pagespeed_Model_Service_Util extends TM_Pagespeed_Model_Service_Abstract
{
    protected $_logFile = "tm_pagespeed_util.log";

    public function getAutodetectedUtilPath()
    {
        $isWindow = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
        $os = $isWindow ? "win32" : (PHP_INT_SIZE === 8 ? "elf64" : "elf32");
        return Mage::getBaseDir('lib') . DS . 'pagespeed' . DS . 'util' .  DS . $os . DS;
    }

    protected function _getUtil($filename)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        if ('jpg' === $extension) {
            $extension = 'jpeg';
        }
        $configPrefix = 'tm_pagespeed/smushit/' . $extension;
        $util = Mage::getStoreConfig($configPrefix . '_util');
        $path = $this->getAutodetectedUtilPath();
        $util = str_replace('{{autodetect_util_path}}', $path, $util);
        $options =  Mage::getStoreConfig($configPrefix . '_util_options');
        $isWindow = strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';

        return $util . ($isWindow ? '.exe' : '') . " " . $options . " " . $filename;
    }

    public function getArchiveFilename($filename)
    {
        return $this->_getArchiveFilename($filename);
    }

    public function smush($filename)
    {
        $filemtime = filemtime($filename);

        $logFilename = $this->_logFile;
        if (!function_exists('exec')) {
            $error = 'php function \'exec\' was disabled';
            Mage::log($error, null, $logFilename);
            throw new Mage_Exception($error, 1);
            return;
        }

        // $sourceSize = filesize($filename);
        $archFilename = $this->_getArchiveFilename($filename);

        if (!file_exists($archFilename)) {
            $username = '';
            if (function_exists('posix_getpwuid')) {
                $user = posix_getpwuid(posix_geteuid());
                $username = $user['name'];
            } else {
                $username = get_current_user();
            }

            if (!is_readable($filename)) {
                $permissions = substr(decoct(fileperms($filename)), 2);
                $fileownerUsername = '';
                if (function_exists('posix_getpwuid')) {
                    $fileOwnerInfo = posix_getpwuid(fileowner($filename));
                    $fileownerUsername = $fileOwnerInfo['name'];
                }

                $error = "Error: File \n\t{$permissions} {$fileownerUsername} {$filename}\n is not readable,\n "
                    . " server is running as {$usename}";
                Mage::log($error, null, $logFilename);
                throw new Mage_Exception($error, 1);
            }

            $dirname = dirname($archFilename);
            if (!is_writable($dirname)) {
                $permissions = substr(decoct(fileperms($dirname)), 2);
                $fileownerUsername = '';
                if (function_exists('posix_getpwuid')) {
                    $fileOwnerInfo = posix_getpwuid(fileowner($filename));
                    $fileownerUsername = $fileOwnerInfo['name'];
                }

                $error = "Error: File \n\t{$permissions} {$fileownerUsername} {$archFilename}\n is not writable,\n "
                    . " server is running as {$username}";
                Mage::log($error, null, $logFilename);
                throw new Mage_Exception($error, 1);
            }

            if (!copy($filename, $archFilename)) {
                return false;
            }
        }
        $run = $this->_getUtil($filename);

        $output = array();
        exec($run, $output, $return);
        if (126 === $return) {
            $error = $run . ' is not executable ' . "\n"
                    . (is_array($output) ? implode("\n", $output) : $output);
            Mage::log($error, null, $logFilename);
            throw new Mage_Exception($error, 1);
        } elseif (127 === $return) {
            $error = $run . ' command  is  not  found ' . "\n"
                    . (is_array($output) ? implode("\n", $output) : $output);
            Mage::log($error, null, $logFilename);
            throw new Mage_Exception($error, 1);
        } else {
            // Zend_Debug::dump($output);
            Mage::log($output, null, $logFilename);
        }

        chmod($archFilename, 0777);
        chmod($filename, 0777);

        $user = posix_getpwuid(posix_geteuid());

        chown($archFilename, $user['name']);
        chown($filename, $user['name']);

        chgrp($archFilename, $user['gid']);
        chgrp($filename, $user['gid']);

        $sourceSize = filesize($archFilename);
        $destinationSize = filesize($filename);

        touch($archFilename, $filemtime, $filemtime);
        touch($filename, $filemtime, $filemtime);

        $percent = (($sourceSize - $destinationSize) * 100) / $sourceSize;
        $data = array(
            'source'    => $archFilename,
            'path'      => $filename,
            'src_size'  => $sourceSize,
            'dest_size' => $destinationSize,
            'percent'   => $percent,
            'smushed'   => $percent > 0 ,
            'created'   => now(),
        );

        return $data;
    }

    public function restore($source, $path)
    {
        return file_exists($source)
            && is_readable($source)
            && is_writable($path)
            && copy($source, $path);
    }

    public function check()
    {
        if (!function_exists('exec')) {
            $error = 'php function \'exec\' was disabled';
            Mage::log($error, null, $this->_logFile);
            throw new Mage_Exception($error, 1);
            return;
        }
        foreach (array('gif', 'jpg', 'png') as $extension) {
            list($filename) = explode(" ", $this->_getUtil("1." . $extension));
            if (!is_executable($filename)) {
                $error = "file {$filename} does not have execute permission";
                Mage::log($error, null, $this->_logFile);
                throw new Mage_Exception($error, 1);
                return;
            }
        }

        return true;
    }
}
